# ---
# Magicat lib sub-project
# ---

include(../defaults.pri)

# Basic declaration
TARGET = tests
TEMPLATE = app

# Qt Config
QT += core testlib
QT -= gui

# Build config
CONFIG += console
CONFIG -= app_bundle
QMAKE_MAC_SDK = macosx10.11

# Sources
SOURCES += main.cpp \
    inputparsertest.cpp \
    csvwritertest.cpp

HEADERS += inputparsertest.h \
    csvwritertest.h

# Linker
LIBS += -L../src -lmagicat
