#ifndef INPUTPARSERTEST_H
#define INPUTPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>

class InputParserTest : public QObject
{
    Q_OBJECT
public:
    explicit InputParserTest(QObject *parent = 0);

private slots:
    void decodeLine();
    void next();
};

#endif // INPUTPARSERTEST_H
