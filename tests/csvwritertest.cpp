#include "csvwritertest.h"
#include "csvwriter.h"

#include <QTextStream>

CsvWriterTest::CsvWriterTest(QObject *parent) :
    QObject(parent)
{
}

void CsvWriterTest::testQuote()
{
    QCOMPARE(CsvWriter::quote("bonjour"), QString("bonjour"));
    QCOMPARE(CsvWriter::quote("\"bonjour\""), QString("\"\"\"bonjour\"\"\""));
    QCOMPARE(CsvWriter::quote("\r"), QString(""));
    QCOMPARE(CsvWriter::quote("\n"), QString("\\n"));
    QCOMPARE(CsvWriter::quote("\r\n"), QString("\\n"));
    QCOMPARE(CsvWriter::quote("bonjour\r\n\"pouet\""), QString("\"bonjour\\n\"\"pouet\"\"\""));
}

void CsvWriterTest::testWriteHeader()
{
    QString written;
    QTextStream stream(&written);
    QStringList columns;

    columns << "b" << "c" << "a";

    CsvWriter writer(&stream, columns);
    writer.writeHeader();

    QCOMPARE(written, QString("a;b;c\n"));
}

void CsvWriterTest::testWriteRow()
{
    QString written;
    QTextStream stream(&written);
    QStringList columns;
    QMap<QString, QString> map;

    columns << "b" << "c" << "a";

    map["a"] = "1";
    map["b"] = "2";
    map["c"] = "3";

    CsvWriter writer(&stream, columns);
    writer.writeRow(&map);

    QCOMPARE(written, QString("1;2;3\n"));
}
