#include <QtTest/QtTest>
#include <QCoreApplication>
#include <algorithm>

#include "inputparsertest.h"
#include "csvwritertest.h"

int main(int argc, char *argv[])
{
    int output = 0;
    QCoreApplication app(argc, argv);

    InputParserTest ipt;
    output += QTest::qExec(&ipt, argc, argv);

    CsvWriterTest cwt;
    output += QTest::qExec(&cwt, argc, argv);

    return std::min(output, 1);
}
