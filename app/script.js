/*jslint browser: true*/
/*globals jQuery,$$magicat,$,console*/

(function ($) {
    'use strict';

    var undef,
        inputs = {
            'lastname': '1.7.3.1.1.5.5',
            'firstname': '1.7.3.1.1.5.11',
            'birth_date': '1.7.3.1.1.5.13.3',
            'submit1': '1.7.3.1.1.7',

            'function': '0.3.5.1.1.1.17.0',
            'address_1': '0.3.5.1.1.1.19.3.9.1.1.1.4.5.1',
            'address_2': '0.3.5.1.1.1.19.3.9.1.1.1.4.5.3',
            'address_zip': '0.3.5.1.1.1.19.3.9.1.1.1.4.9.3.1.1',
            'address_city': '0.3.5.1.1.1.19.3.9.1.1.1.4.11.0',
            'address_country': '0.3.5.1.1.1.19.3.9.1.1.1.4.13.3.1.1',
            'email': '0.3.5.1.1.1.25.1',
            'cgu': '0.3.5.1.1.1.29',
            'allow_data': '0.3.5.1.1.1.37',

            'company_btn': '#cktl_e_0_3_5_1_1_1_21_1_5_3_1_btn',
            'company_search': '0.3.5.1.1.1.21.1.5.3.3.0.1.0.1.1.3.9.1.0',
            'company_search_btn': '#cktl_e_0_3_5_1_1_1_21_1_5_3_3_0_1_0_1_1_3_13_btn',
            'company_search_cancel': '#cktl_e_0_3_5_1_1_1_21_1_5_3_3_0_1_0_1_3_3_btn',

            'company_add_btn': '#cktl_e_0_3_5_1_1_1_21_1_3_btn',
            'company_address_add': '#cktl_e_0_3_5_1_1_1_21_1_5_5_11_7_1_1_1_1_1_btn',
            'company_name': '0.3.5.1.1.1.21.1.5.1.3',
            'company_address_1': '0.3.5.1.1.1.21.1.5.5.11.9.1.1.1.4.5.1',
            'company_address_2': '0.3.5.1.1.1.21.1.5.5.11.9.1.1.1.4.5.3',
            'company_address_bp': '0.3.5.1.1.1.21.1.5.5.11.9.1.1.1.4.7.0',
            'company_address_zip': '0.3.5.1.1.1.21.1.5.5.11.9.1.1.1.4.9.3.1.1',
            'company_address_city': '0.3.5.1.1.1.21.1.5.5.11.9.1.1.1.4.11.0',
            'company_address_country': '0.3.5.1.1.1.21.1.5.5.11.9.1.1.1.4.13.3.1.1',

            'phone_btn': '#cktl_e_0_3_5_1_1_1_19_5_1_1_1_1_1_1_1_btn',
            'phone_add_btn': '#cktl_e_0_3_5_1_1_1_19_5_3_13_1_btn',
            'phone': '0.3.5.1.1.1.19.5.3.11.1'
        },

        getInput,
        updateInput,
        clickRadio,
        checkCheckbox,
        router,
        fillPhone,
        searchCompany,
        addCompany,
        step1,
        step2,
        step3;

    getInput = function (name) {
        return $('input[name="' + inputs[name] + '"]');
    };

    updateInput = function (name, value) {
        var event = document.createEvent('HTMLEvents'),
            element = getInput(name);

        console.log('update input ' + name + ', set value "' + value + '"');

        event.initEvent('change', false, true);
        element.val(value);

        if (element[0] !== undef) {
            element[0].dispatchEvent(event);
        }
    };

    clickRadio = function (selector) {
        var element = $(selector);
        element.trigger('click');
    };

    checkCheckbox = function (name) {
        var checkbox = $('input[value="' + inputs[name] + '"]');
        checkbox.attr('checked', 'checked');
    };

    router = function (url) {
        var URL_STEP_1 = /https:\/\/apps\.univ-lr\.fr\/cgi-bin\/WebObjects\/RA(?:TEST)?\.woa\?passDlg=9Qt9WgNxVg4M68z2&forceProfil=E/,
            URL_STEP_2 = /https:\/\/apps\.univ-lr\.fr\/cgi-bin\/WebObjects\/RA(?:TEST)?\.woa\/1\/[\w\W]*\/0\.1\.7\.3\.1/,
            URL_STEP_3 = /https:\/\/apps\.univ-lr\.fr\/cgi-bin\/WebObjects\/RA(?:TEST)?\.woa\/1\/[\w\W]*\/\d+\.0\.3\.5\.1\.1\.1/;


        if (URL_STEP_1.test(url)) {
            step1();
        } else if (URL_STEP_2.test(url)) {
            step2();
        } else if (URL_STEP_3.test(url)) {
            step3();
        }
    };

    fillPhone = function () {
        var interval;
        $(inputs.phone_btn).click();

        interval = setInterval(function () {
            var input = getInput('phone');

            if (input.length > 0) {
                clearInterval(interval);

                setTimeout(function () {
                    updateInput('phone', $$magicat.data.phone);
                    clickRadio('#e_0_3_5_1_1_1_19_5_3_5 input[value="N"]');

                    setTimeout(function () {
                        $(inputs.phone_add_btn).click();
                        searchCompany();
                    }, 1500);
                }, 10);
            }
        }, 10);
    };

    searchCompany = function () {
        var interval;

        if ($$magicat.data.company_name !== '') {
            console.log('trying to fill the company');
            $('#RadioEmploiOui').attr('checked', 'checked').click();

            interval = setInterval(function () {
                var btn = $(inputs.company_btn);

                if (btn.length > 0) {
                    clearInterval(interval);
                    btn.click();

                    interval = setInterval(function () {
                        var input = getInput('company_search'),
                            searchBtn = $(inputs.company_search_btn);

                        if (input.length > 0) {
                            clearInterval(interval);

                            setTimeout(function () {
                                updateInput('company_search', $$magicat.data.company_name);
                            }, 2000);


                            setTimeout(function () {
                                searchBtn.click();

                                setTimeout(function () {
                                    var results = $('form[name=FormEntreprise]').find('div[id*=_ListeResContainer]').text().strip();
                                    if (results === 'Aucune personne trouvée.') {
                                        console.log('no company found, falling back to add');
                                        addCompany();
                                    }
                                }, 3000);
                            }, 3100);
                        }
                    }, 10);
                }
            }, 10);
        } else {
            $('#RadioEmploiNon').attr('checked', 'checked');
        }
    };

    addCompany = function () {
        var closeSearch, clickAddCompany, addCompanyAddress, fillCompanyAddress, uncheckEmployment;

        closeSearch = function () {
            /*globals Windows*/
            Windows.close("FenetreEntreprise_win", event);

            if ($$magicat.data.company_address_1) {
                setTimeout(clickAddCompany, 1000);
            } else {
                uncheckEmployment();
            }
        };

        clickAddCompany = function () {
            $(inputs.company_add_btn).click();
            setTimeout(addCompanyAddress, 1000);
        };

        addCompanyAddress = function () {
            updateInput('company_name', $$magicat.data.company_name);
            $(inputs.company_address_add).click();
            setTimeout(fillCompanyAddress, 1000);
        };

        fillCompanyAddress = function () {
            var fields = ['1', '2', 'bp', 'zip', 'city', 'country'], i, fieldName;

            for (i = 0; i < fields.length; i += 1) {
                fieldName = 'company_address_' + fields[i];
                updateInput(fieldName, $$magicat.data[fieldName]);
            }
        };

        uncheckEmployment = function () {
            $('#RadioEmploiNon').attr('checked', 'checked').click();
        };

        closeSearch();
    };

    step1 = function () {
        getInput('firstname').val($$magicat.data.firstname);
        getInput('lastname').val($$magicat.data.lastname);
        getInput('birth_date').val($$magicat.data.birth_date);
        getInput('submit1').click();
    };

    step2 = function () {
        var failMessage = ($('#login_error_msg').text() || '').trim().replace(/[\s\r\n\t]+/gm, ' ');

        console.log('step 2');

        if (failMessage !== '') {
            $$magicat.fail(failMessage);
            return;
        }

        updateInput('function', $$magicat.data['function']);
        updateInput('address_1', $$magicat.data.address_1);
        updateInput('address_2', $$magicat.data.address_2);
        updateInput('address_zip', $$magicat.data.address_zip);
        updateInput('address_city', $$magicat.data.address_city);
        updateInput('address_country', $$magicat.data.address_country);
        updateInput('email', $$magicat.data.email);

        if ($$magicat.data.allow_data === 'Oui') {
            checkCheckbox('allow_data');
        }

        checkCheckbox('cgu');

        fillPhone();
    };

    step3 = function () {
        if ($('form[name="FormInsc"]').length === 0) {
            $$magicat.finish();
        }
    };

    (function () {
        router(window.location.toString());
    }());
}(jQuery));
