# ---
# Magicat app sub-project
# ---

include(../defaults.pri)

# Basic declaration
TARGET = MagiCat
TEMPLATE = app

CONFIG += c++11
QMAKE_MAC_SDK = macosx10.11

# Icon
ICON = icons/icon.icns

# Qt Config
QT       += core gui webkitwidgets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# Source files
SOURCES += main.cpp\
        fillwindow.cpp

HEADERS  += fillwindow.h

FORMS    += fillwindow.ui

# Linker
LIBS += -L../src -lmagicat

RESOURCES += \
    static.qrc
