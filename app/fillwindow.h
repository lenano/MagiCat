#ifndef FILLWINDOW_H
#define FILLWINDOW_H

#include <QMainWindow>
#include <QUrl>
#include <QSettings>

#include "manager.h"
#include "dataset.h"
#include "mapmodel.h"
#include "magicatjs.h"

namespace Ui {
class FillWindow;
}

class FillWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FillWindow(QWidget *parent = 0);
    ~FillWindow();

private slots:
    void updateDataset(Dataset *dataset);
    void injectJS();
    void setUrl(QString url);
    void loadScript(QString filename);
    void loadData(QString filename);
    void finishEntry();
    void promptFailLogFile();
    void reportFail(QString logEntry);
    void sayDone();

    void on_actionLoadUserURL_triggered();
    void on_webView_urlChanged(const QUrl &arg1);
    void on_actionLoad_triggered();
    void on_actionQuit_triggered();
    void on_actionLoad_script_triggered();
    void on_webView_loadFinished(bool ok);
    void on_actionSet_Initial_URL_triggered();
    void on_resetButton_clicked();
    void on_validateButton_clicked();
    void on_actionSet_fail_log_location_triggered();
    void on_failButton_clicked();
    void on_actionCat_triggered();

private:
    Ui::FillWindow *ui;
    QSettings settings;

    Manager manager;
    Dataset *currentDataset;
    MapModel mapModel;
    QString script;
    MagicatJS *magicatJS;
    QString failLogFile;

    // Settings
    QString initUrl;
    QString scriptName;
    QString dataFile;
};

#endif // FILLWINDOW_H
