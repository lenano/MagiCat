#include "fillwindow.h"
#include <QApplication>
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("ActivKonnect");
    QCoreApplication::setOrganizationDomain("aksrv.net");
    QCoreApplication::setApplicationName("MagiCat");

    QApplication a(argc, argv);
    FillWindow w;
    w.show();

    return a.exec();
}
