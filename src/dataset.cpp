#include "dataset.h"

Dataset::Dataset(int index, QMap<QString, QString> *data, QObject *parent) :
    QObject(parent), index(index), data(data)
{
}

int Dataset::getIndex() const
{
    return index;
}

void Dataset::setIndex(int value)
{
    index = value;
}

QMap<QString, QString> *Dataset::getData() const
{
    return data;
}

void Dataset::setData(QMap<QString, QString> *value)
{
    data = value;
}


