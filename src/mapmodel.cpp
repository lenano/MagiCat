#include "mapmodel.h"

#include <QVariant>
#include <QtDebug>

MapModel::MapModel(QObject *parent) :
    QStandardItemModel(0, 2, parent),
    mapData(NULL),
    oldLength(0)
{
}

MapModel::MapModel(QMap<QString, QString> *mapData, QObject *parent) :
    QStandardItemModel(0, 2, parent),
    mapData(mapData)
{
    update();
}

void MapModel::update()
{
    if (mapData == NULL) {
        return;
    }

    setHeaderData(0, Qt::Horizontal, "Champ");
    setHeaderData(1, Qt::Horizontal, "Valeur");

    setRowCount(mapData->size());

    QMapIterator<QString, QString> i(*mapData);
    int pos = 0;
    while(i.hasNext()) {
        i.next();
        setData(index(pos, 0), i.key());
        setData(index(pos, 1), i.value());
        pos += 1;
    }
}

void MapModel::update(QMap<QString, QString> *mapData)
{
    this->mapData = mapData;
    update();
}

QMap<QString, QString> *MapModel::getMapData() const
{
    return mapData;
}

void MapModel::setMapData(QMap<QString, QString> *value)
{
    mapData = value;
}
