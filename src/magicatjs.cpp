#include "magicatjs.h"

#include <QDebug>

MagicatJS::MagicatJS(QObject *parent) :
    QObject(parent)
{
}

QVariantMap MagicatJS::getData() const
{
    return data;
}

void MagicatJS::setData(const QVariantMap &value)
{
    data = value;
}

void MagicatJS::updateData(QMap<QString, QString> *data)
{
    if (data != NULL) {
        QMap<QString, QString>::const_iterator i;
        for (i = data->constBegin(); i != data->constEnd(); ++i) {
            this->data[i.key()] = i.value();
        }
    }
}

void MagicatJS::finish()
{
    emit finished();
}

void MagicatJS::fail(const QString &reason)
{
    emit failed(reason);
}
