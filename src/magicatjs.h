#ifndef MAGICATJS_H
#define MAGICATJS_H

#include <QObject>
#include <QMap>
#include <QVariantMap>

class MagicatJS : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap data READ getData WRITE setData)

public:
    explicit MagicatJS(QObject *parent = 0);

    QVariantMap getData() const;
    void setData(const QVariantMap &value);

signals:
    void finished();
    void failed(const QString &reason);

public slots:
    void updateData(QMap<QString, QString> *data);
    void finish();
    void fail(const QString &reason);

private:
    QVariantMap data;
};

#endif // MAGICATJS_H
