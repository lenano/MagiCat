#ifndef DATASET_H
#define DATASET_H

#include <QObject>
#include <QMap>
#include <QString>

class Dataset : public QObject
{
    Q_OBJECT
public:
    explicit Dataset(int index, QMap<QString, QString> *data,
                     QObject *parent = 0);
    int getIndex() const;
    void setIndex(int value);

    QMap<QString, QString> *getData() const;
    void setData(QMap<QString, QString> *value);

private:
    int index;
    QMap<QString, QString> *data;
};

#endif // DATASET_H
