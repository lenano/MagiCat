#include "manager.h"
#include "inputparser.h"

#include <QDebug>
#include <QVariantList>
#include <QCryptographicHash>

Manager::Manager(QObject *parent) :
    QObject(parent)
{
}

void Manager::load(QString filename)
{
    QFile input(filename);

    if (input.open(QFile::ReadOnly)) {
        QTextStream stream(&input);
        InputParser parser(&stream, this);

        this->filename = filename;
        generatedSettingsKey = settingsKey(filename);

        data.clear();

        while (true) {
            QMap<QString, QString> row = parser.next();

            if (row.isEmpty()) {
                break;
            }

            todo << data.size();
            data << row;
        }

        if (settings.contains(generatedSettingsKey)) {
            todoFromQVAriant(settings.value(generatedSettingsKey));
        }
    }

    sendDataset();
}

void Manager::markDone(Dataset *dataset)
{
    todo.remove(dataset->getIndex());
    dataset->deleteLater();
    sendDataset();

    if (!generatedSettingsKey.isEmpty()) {
        settings.setValue(generatedSettingsKey, todoToQVariant());
    }
}

void Manager::sendDataset()
{
    QSetIterator<int> i(todo);

    if (i.hasNext()) {
        int index = i.next();
        emit changedDataset(new Dataset(index, &data[index], this));
    } else {
        emit done();
        emit changedDataset(new Dataset(0, new QMap<QString, QString>(), this));
    }

    emitProgress();
}

void Manager::todoFromQVAriant(QVariant variantTodo)
{
    QVariantList list = variantTodo.toList();
    todo.clear();

    foreach (QVariant item, list) {
        todo << item.toInt();
    }
}

QVariant Manager::todoToQVariant()
{
    QVariantList output;

    foreach (int item, todo) {
        output << item;
    }

    return QVariant(output);
}

QString Manager::settingsKey(QString filename)
{
    QFile f(filename);

    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(QCryptographicHash::Sha256);

        if (hash.addData(&f)) {
            return "input_files/todo/" + QString(hash.result().toHex());
        }
    }

    return "input_files/todo/noope";
}

void Manager::emitProgress()
{
    float currentProgress = 1.0 - ((float)(todo.size()) / (float)(data.size()));
    emit progress(currentProgress * 10000.0);
}
