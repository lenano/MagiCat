#ifndef MAPMODEL_H
#define MAPMODEL_H

#include <QStandardItemModel>
#include <QMap>
#include <QStringList>

class MapModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit MapModel(QObject *parent = 0);
    explicit MapModel(QMap<QString, QString> *mapData, QObject *parent = 0);

public:
    void update();
    void update(QMap<QString, QString> *mapData);

private:
    QMap<QString, QString> *mapData;
    QStringList fields;
    int oldLength;

public:
    QMap<QString, QString> *getMapData() const;
    void setMapData(QMap<QString, QString> *value);
};

#endif // MAPMODEL_H
