#include <QStringList>
#include <algorithm>

#include "inputparser.h"

const QChar InputParser::separator = QChar(';');
const QChar InputParser::quote = QChar('"');

InputParser::InputParser(QTextStream *input, QObject *parent) :
    QObject(parent), input(input)
{
}

QStringList InputParser::decodeLine() {
    QStringList output;
    QString line = input->readLine();

    if (line == NULL) {
        return output;
    }

    QString buf;
    bool inQuote = false;

    for (int i = 0; i < line.length(); i += 1) {
        QChar c0 = line[i], c1, cp;

        if (inQuote) {
            if (c0 == quote) {
                if (i + 1 < line.length()) {
                    c1 = line[i + 1];
                }

                if (i > 0) {
                    cp = line[i - 1];
                }

                if (c1 == quote) {
                    buf += c0;
                } else if (cp != quote) {
                    inQuote = false;
                }
            } else {
                buf += c0;
            }
        } else {
            if (c0 == separator) {
                output << buf;
                buf = "";
            } else if (c0 == quote) {
                inQuote = true;
            } else {
                buf += c0;
            }
        }

        if (inQuote && i + 1 == line.length()) {
            buf += "\n";
            line += input->readLine();
        }
    }

    output << buf;

    return output;
}

QMap<QString, QString> InputParser::next() {
    QMap<QString, QString> output;
    QStringList line = decodeLine();

    if (!line.isEmpty()) {
        if (columns.isEmpty()) {
            columns = line;
            return next();
        } else {
            int len = std::min(columns.length(), line.length());
            for (int i = 0; i < len; i += 1) {
                output[columns[i]] = line[i];
            }
        }
    }

    return output;
}
