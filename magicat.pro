TEMPLATE = subdirs
CONFIG += ordered
CONFIG += c++11
QMAKE_MAC_SDK = macosx10.11

# Icon
ICON = icons/icon.icns

SUBDIRS += \
    src \
    app \
    tests

app.depends = src
tests.depends = src
